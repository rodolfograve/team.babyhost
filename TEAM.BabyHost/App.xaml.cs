﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;

namespace TEAM.BabyHost
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected Controller Controller;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Controller = new Controller();
            Controller.Start();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            Controller.Dispose();
            base.OnExit(e);
        }

    }
}
