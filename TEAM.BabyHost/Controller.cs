﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WinForms = System.Windows.Forms;

namespace TEAM.BabyHost
{
    public class Controller : IDisposable
    {

        public Controller()
        {
            _proc = KeyboardHookCallback;
        }

        private readonly KeyboardHook.LowLevelKeyboardProc _proc;
        private static IntPtr _hookID = IntPtr.Zero;

        public void Start()
        {
            try
            {
                _hookID = KeyboardHook.SetHook(_proc);
            }
            catch (Exception ex)
            {
                Trace.TraceError("Cannot hook into keyboard events: " + ex);
                if (_hookID != IntPtr.Zero)
                {
                    KeyboardHook.UnhookWindowsHookEx(_hookID);
                }
                throw;
            }
            PlayingWindow = new PlayingWindow();
            PlayingWindow.Controller = this;
            PlayingWindow.Show();
        }

        public PlayingWindow PlayingWindow { get; set; }

        public void StartPlaying(string gameUri)
        {
            try
            {
                var uri = new Uri(gameUri);
                PlayingWindow.LoadUri(uri);
                IsPlaying = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error loading the specified URI: " + ex);
            }
        }

        public void StopPlaying()
        {
            PlayingWindow.UnloadUri();
        }

        public bool IsPlaying { get; protected set; }

        public IntPtr KeyboardHookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (IsPlaying && nCode >= 0)
            {
                bool Alt = (WinForms.Control.ModifierKeys & WinForms.Keys.Alt) != 0;
                bool Control = (WinForms.Control.ModifierKeys & WinForms.Keys.Control) != 0;

                //Prevent ALT-TAB and CTRL-ESC by eating TAB and ESC. Also kill Windows WinForms.Keys.
                int vkCode = Marshal.ReadInt32(lParam);
                var key = (WinForms.Keys)vkCode;

                if (Alt && key == WinForms.Keys.F4)
                {
                    //Application.Current.Shutdown();
                    return (IntPtr)1; //handled
                }
                if (key == WinForms.Keys.LWin || key == WinForms.Keys.RWin) return (IntPtr)1; //handled
                if (Alt && key == WinForms.Keys.Tab) return (IntPtr)1; //handled
                if (Alt && key == WinForms.Keys.Space) return (IntPtr)1; //handled
                if (Control && key == WinForms.Keys.Escape) return (IntPtr)1;
                if (key == WinForms.Keys.None) return (IntPtr)1; //handled
                if (key <= WinForms.Keys.Back) return (IntPtr)1; //handled
                if (key == WinForms.Keys.Menu) return (IntPtr)1; //handled
                if (key == WinForms.Keys.Pause) return (IntPtr)1; //handled
                if (key == WinForms.Keys.Help) return (IntPtr)1; //handled
                if (key == WinForms.Keys.Sleep) return (IntPtr)1; //handled
                if (key == WinForms.Keys.Apps) return (IntPtr)1; //handled
                if (key >= WinForms.Keys.KanaMode && key <= WinForms.Keys.HanjaMode) return (IntPtr)1; //handled
                if (key >= WinForms.Keys.IMEConvert && key <= WinForms.Keys.IMEModeChange) return (IntPtr)1; //handled
                if (key >= WinForms.Keys.BrowserBack && key <= WinForms.Keys.BrowserHome) return (IntPtr)1; //handled
                if (key >= WinForms.Keys.MediaNextTrack && key <= WinForms.Keys.OemClear) return (IntPtr)1; //handled

                Debug.WriteLine(vkCode.ToString() + " " + key);
            }
            return KeyboardHook.CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        public void Dispose()
        {
            if (_hookID != IntPtr.Zero)
            {
                KeyboardHook.UnhookWindowsHookEx(_hookID);
            }
        }

    }
}
