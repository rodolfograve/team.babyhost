﻿using System;
using System.Windows;
using System.Windows.Input;

namespace TEAM.BabyHost
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class PlayingWindow : Window
    {
        public PlayingWindow()
        {
            InitializeComponent();
            UriToLoad.Focus();
            Browser.PreviewKeyDown += Browser_PreviewKeyDown;
            Browser.PreviewKeyUp += Browser_PreviewKeyUp;
            Browser.KeyDown += Browser_KeyDown;
            Browser.KeyUp += Browser_KeyUp;
        }

        protected void ProcessKeyEvent(System.Windows.Input.KeyEventArgs e)
        {
            if (
                Keyboard.Modifiers.HasFlag(ModifierKeys.Control) &&
                Keyboard.Modifiers.HasFlag(ModifierKeys.Shift) &&
                Keyboard.Modifiers.HasFlag(ModifierKeys.Alt))
            {
                if (e.Key == Key.End)
                {
                    Controller.StopPlaying();
                    e.Handled = true;
                }
            }
            else if (
                Keyboard.Modifiers.HasFlag(ModifierKeys.Control) ||
                Keyboard.Modifiers.HasFlag(ModifierKeys.Shift) ||
                Keyboard.Modifiers.HasFlag(ModifierKeys.Alt) ||
                Keyboard.Modifiers.HasFlag(ModifierKeys.Windows))
            {
                e.Handled = true;
            }
        }

        protected void Browser_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ProcessKeyEvent(e);
        }

        protected void Browser_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ProcessKeyEvent(e);
        }

        protected void Browser_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ProcessKeyEvent(e);
        }

        protected void Browser_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            ProcessKeyEvent(e);
        }

        public Controller Controller { get; set; }

        private void StartPlaying_Click(object sender, RoutedEventArgs e)
        {
            Controller.StartPlaying(UriToLoad.Text);
        }

        public void LoadUri(Uri uri)
        {
            WindowStyle = System.Windows.WindowStyle.None;
            WindowState = System.Windows.WindowState.Maximized;
            ResizeMode = System.Windows.ResizeMode.NoResize;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            UriToLoadText.Text = "Press Shift + Ctrl + Alt + End";
            UriToLoad.Visibility = System.Windows.Visibility.Collapsed;
            Play.Visibility = System.Windows.Visibility.Collapsed;
            Browser.Visibility = System.Windows.Visibility.Visible;
            Browser.Focus();
            Browser.Source = uri;
        }

        public void UnloadUri()
        {
            Browser.Source = null;
            Browser.Visibility = System.Windows.Visibility.Hidden;
            WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
            WindowState = System.Windows.WindowState.Normal;
            ResizeMode = System.Windows.ResizeMode.CanResize & System.Windows.ResizeMode.CanMinimize;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            UriToLoadText.Text = "Load URI: ";
            UriToLoad.Visibility = System.Windows.Visibility.Visible;
            Play.Visibility = System.Windows.Visibility.Visible;
            UriToLoad.Focus();
        }

        private void UriToLoad_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Controller.StartPlaying(UriToLoad.Text);
            }
        }

    }
}
